//
//  ViewController.swift
//  PerfectScroll
//
//  Created by mac on 10/8/18.
//  Copyright © 2018 Brahim Bouhouche. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource {
    
    @IBAction func darkMode(_ sender: Any) {
        print("to do printed")
        
        view.backgroundColor = UIColor.darkGray
        let views = view.subviews
        for part in views {
            if part is UILabel {
                let currentLabel = part as! UILabel
                currentLabel.textColor = UIColor.white
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1000
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let cell = tableView.dequeueReusableCell(withIdentifier: "coolCell", for: indexPath)
        
        cell.textLabel?.text = "This row number \(indexPath.row) the section \(indexPath.section) "
        cell.accessoryType = .detailDisclosureButton
        return cell
    }
    

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

